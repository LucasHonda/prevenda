package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.LinkView;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.Property;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("all")
public class produtoRealmRealmProxy extends br.com.stts.prevenda.produtoRealm
    implements RealmObjectProxy, produtoRealmRealmProxyInterface {

    static final class produtoRealmColumnInfo extends ColumnInfo {
        long codigoIndex;
        long descricaoIndex;
        long precoIndex;
        long imagemIndex;

        produtoRealmColumnInfo(SharedRealm realm, Table table) {
            super(4);
            this.codigoIndex = addColumnDetails(table, "codigo", RealmFieldType.STRING);
            this.descricaoIndex = addColumnDetails(table, "descricao", RealmFieldType.STRING);
            this.precoIndex = addColumnDetails(table, "preco", RealmFieldType.STRING);
            this.imagemIndex = addColumnDetails(table, "imagem", RealmFieldType.INTEGER);
        }

        produtoRealmColumnInfo(ColumnInfo src, boolean mutable) {
            super(src, mutable);
            copy(src, this);
        }

        @Override
        protected final ColumnInfo copy(boolean mutable) {
            return new produtoRealmColumnInfo(this, mutable);
        }

        @Override
        protected final void copy(ColumnInfo rawSrc, ColumnInfo rawDst) {
            final produtoRealmColumnInfo src = (produtoRealmColumnInfo) rawSrc;
            final produtoRealmColumnInfo dst = (produtoRealmColumnInfo) rawDst;
            dst.codigoIndex = src.codigoIndex;
            dst.descricaoIndex = src.descricaoIndex;
            dst.precoIndex = src.precoIndex;
            dst.imagemIndex = src.imagemIndex;
        }
    }

    private produtoRealmColumnInfo columnInfo;
    private ProxyState<br.com.stts.prevenda.produtoRealm> proxyState;
    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("codigo");
        fieldNames.add("descricao");
        fieldNames.add("preco");
        fieldNames.add("imagem");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    produtoRealmRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (produtoRealmColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<br.com.stts.prevenda.produtoRealm>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$codigo() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.codigoIndex);
    }

    @Override
    public void realmSet$codigo(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.codigoIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.codigoIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.codigoIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.codigoIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$descricao() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.descricaoIndex);
    }

    @Override
    public void realmSet$descricao(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'descricao' to null.");
            }
            row.getTable().setString(columnInfo.descricaoIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'descricao' to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.descricaoIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$preco() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.precoIndex);
    }

    @Override
    public void realmSet$preco(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'preco' to null.");
            }
            row.getTable().setString(columnInfo.precoIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'preco' to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.precoIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public int realmGet$imagem() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.imagemIndex);
    }

    @Override
    public void realmSet$imagem(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.imagemIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.imagemIndex, value);
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        OsObjectSchemaInfo.Builder builder = new OsObjectSchemaInfo.Builder("produtoRealm");
        builder.addProperty("codigo", RealmFieldType.STRING, !Property.PRIMARY_KEY, Property.INDEXED, !Property.REQUIRED);
        builder.addProperty("descricao", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addProperty("preco", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addProperty("imagem", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
         return expectedObjectSchemaInfo;
    }

    public static produtoRealmColumnInfo validateTable(SharedRealm sharedRealm, boolean allowExtraColumns) {
        if (!sharedRealm.hasTable("class_produtoRealm")) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "The 'produtoRealm' class is missing from the schema for this Realm.");
        }
        Table table = sharedRealm.getTable("class_produtoRealm");
        final long columnCount = table.getColumnCount();
        if (columnCount != 4) {
            if (columnCount < 4) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is less than expected - expected 4 but was " + columnCount);
            }
            if (allowExtraColumns) {
                RealmLog.debug("Field count is more than expected - expected 4 but was %1$d", columnCount);
            } else {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is more than expected - expected 4 but was " + columnCount);
            }
        }
        Map<String, RealmFieldType> columnTypes = new HashMap<String, RealmFieldType>();
        for (long i = 0; i < columnCount; i++) {
            columnTypes.put(table.getColumnName(i), table.getColumnType(i));
        }

        final produtoRealmColumnInfo columnInfo = new produtoRealmColumnInfo(sharedRealm, table);

        if (table.hasPrimaryKey()) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Primary Key defined for field " + table.getColumnName(table.getPrimaryKey()) + " was removed.");
        }

        if (!columnTypes.containsKey("codigo")) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'codigo' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
        }
        if (columnTypes.get("codigo") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'codigo' in existing Realm file.");
        }
        if (!table.isColumnNullable(columnInfo.codigoIndex)) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'codigo' is required. Either set @Required to field 'codigo' or migrate using RealmObjectSchema.setNullable().");
        }
        if (!table.hasSearchIndex(table.getColumnIndex("codigo"))) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Index not defined for field 'codigo' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
        }
        if (!columnTypes.containsKey("descricao")) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'descricao' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
        }
        if (columnTypes.get("descricao") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'descricao' in existing Realm file.");
        }
        if (table.isColumnNullable(columnInfo.descricaoIndex)) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'descricao' does support null values in the existing Realm file. Remove @Required or @PrimaryKey from field 'descricao' or migrate using RealmObjectSchema.setNullable().");
        }
        if (!columnTypes.containsKey("preco")) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'preco' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
        }
        if (columnTypes.get("preco") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'preco' in existing Realm file.");
        }
        if (table.isColumnNullable(columnInfo.precoIndex)) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'preco' does support null values in the existing Realm file. Remove @Required or @PrimaryKey from field 'preco' or migrate using RealmObjectSchema.setNullable().");
        }
        if (!columnTypes.containsKey("imagem")) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'imagem' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
        }
        if (columnTypes.get("imagem") != RealmFieldType.INTEGER) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'int' for field 'imagem' in existing Realm file.");
        }
        if (table.isColumnNullable(columnInfo.imagemIndex)) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'imagem' does support null values in the existing Realm file. Use corresponding boxed type for field 'imagem' or migrate using RealmObjectSchema.setNullable().");
        }

        return columnInfo;
    }

    public static String getTableName() {
        return "class_produtoRealm";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static br.com.stts.prevenda.produtoRealm createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        br.com.stts.prevenda.produtoRealm obj = realm.createObjectInternal(br.com.stts.prevenda.produtoRealm.class, true, excludeFields);
        if (json.has("codigo")) {
            if (json.isNull("codigo")) {
                ((produtoRealmRealmProxyInterface) obj).realmSet$codigo(null);
            } else {
                ((produtoRealmRealmProxyInterface) obj).realmSet$codigo((String) json.getString("codigo"));
            }
        }
        if (json.has("descricao")) {
            if (json.isNull("descricao")) {
                ((produtoRealmRealmProxyInterface) obj).realmSet$descricao(null);
            } else {
                ((produtoRealmRealmProxyInterface) obj).realmSet$descricao((String) json.getString("descricao"));
            }
        }
        if (json.has("preco")) {
            if (json.isNull("preco")) {
                ((produtoRealmRealmProxyInterface) obj).realmSet$preco(null);
            } else {
                ((produtoRealmRealmProxyInterface) obj).realmSet$preco((String) json.getString("preco"));
            }
        }
        if (json.has("imagem")) {
            if (json.isNull("imagem")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'imagem' to null.");
            } else {
                ((produtoRealmRealmProxyInterface) obj).realmSet$imagem((int) json.getInt("imagem"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static br.com.stts.prevenda.produtoRealm createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        br.com.stts.prevenda.produtoRealm obj = new br.com.stts.prevenda.produtoRealm();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (false) {
            } else if (name.equals("codigo")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((produtoRealmRealmProxyInterface) obj).realmSet$codigo(null);
                } else {
                    ((produtoRealmRealmProxyInterface) obj).realmSet$codigo((String) reader.nextString());
                }
            } else if (name.equals("descricao")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((produtoRealmRealmProxyInterface) obj).realmSet$descricao(null);
                } else {
                    ((produtoRealmRealmProxyInterface) obj).realmSet$descricao((String) reader.nextString());
                }
            } else if (name.equals("preco")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((produtoRealmRealmProxyInterface) obj).realmSet$preco(null);
                } else {
                    ((produtoRealmRealmProxyInterface) obj).realmSet$preco((String) reader.nextString());
                }
            } else if (name.equals("imagem")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'imagem' to null.");
                } else {
                    ((produtoRealmRealmProxyInterface) obj).realmSet$imagem((int) reader.nextInt());
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        obj = realm.copyToRealm(obj);
        return obj;
    }

    public static br.com.stts.prevenda.produtoRealm copyOrUpdate(Realm realm, br.com.stts.prevenda.produtoRealm object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().threadId != realm.threadId) {
            throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
        }
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return object;
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (br.com.stts.prevenda.produtoRealm) cachedRealmObject;
        }

        return copy(realm, object, update, cache);
    }

    public static br.com.stts.prevenda.produtoRealm copy(Realm realm, br.com.stts.prevenda.produtoRealm newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (br.com.stts.prevenda.produtoRealm) cachedRealmObject;
        }

        // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
        br.com.stts.prevenda.produtoRealm realmObject = realm.createObjectInternal(br.com.stts.prevenda.produtoRealm.class, false, Collections.<String>emptyList());
        cache.put(newObject, (RealmObjectProxy) realmObject);

        produtoRealmRealmProxyInterface realmObjectSource = (produtoRealmRealmProxyInterface) newObject;
        produtoRealmRealmProxyInterface realmObjectCopy = (produtoRealmRealmProxyInterface) realmObject;

        realmObjectCopy.realmSet$codigo(realmObjectSource.realmGet$codigo());
        realmObjectCopy.realmSet$descricao(realmObjectSource.realmGet$descricao());
        realmObjectCopy.realmSet$preco(realmObjectSource.realmGet$preco());
        realmObjectCopy.realmSet$imagem(realmObjectSource.realmGet$imagem());
        return realmObject;
    }

    public static long insert(Realm realm, br.com.stts.prevenda.produtoRealm object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(br.com.stts.prevenda.produtoRealm.class);
        long tableNativePtr = table.getNativePtr();
        produtoRealmColumnInfo columnInfo = (produtoRealmColumnInfo) realm.schema.getColumnInfo(br.com.stts.prevenda.produtoRealm.class);
        long rowIndex = OsObject.createRow(table);
        cache.put(object, rowIndex);
        String realmGet$codigo = ((produtoRealmRealmProxyInterface) object).realmGet$codigo();
        if (realmGet$codigo != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.codigoIndex, rowIndex, realmGet$codigo, false);
        }
        String realmGet$descricao = ((produtoRealmRealmProxyInterface) object).realmGet$descricao();
        if (realmGet$descricao != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.descricaoIndex, rowIndex, realmGet$descricao, false);
        }
        String realmGet$preco = ((produtoRealmRealmProxyInterface) object).realmGet$preco();
        if (realmGet$preco != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.precoIndex, rowIndex, realmGet$preco, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.imagemIndex, rowIndex, ((produtoRealmRealmProxyInterface) object).realmGet$imagem(), false);
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(br.com.stts.prevenda.produtoRealm.class);
        long tableNativePtr = table.getNativePtr();
        produtoRealmColumnInfo columnInfo = (produtoRealmColumnInfo) realm.schema.getColumnInfo(br.com.stts.prevenda.produtoRealm.class);
        br.com.stts.prevenda.produtoRealm object = null;
        while (objects.hasNext()) {
            object = (br.com.stts.prevenda.produtoRealm) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            long rowIndex = OsObject.createRow(table);
            cache.put(object, rowIndex);
            String realmGet$codigo = ((produtoRealmRealmProxyInterface) object).realmGet$codigo();
            if (realmGet$codigo != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.codigoIndex, rowIndex, realmGet$codigo, false);
            }
            String realmGet$descricao = ((produtoRealmRealmProxyInterface) object).realmGet$descricao();
            if (realmGet$descricao != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.descricaoIndex, rowIndex, realmGet$descricao, false);
            }
            String realmGet$preco = ((produtoRealmRealmProxyInterface) object).realmGet$preco();
            if (realmGet$preco != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.precoIndex, rowIndex, realmGet$preco, false);
            }
            Table.nativeSetLong(tableNativePtr, columnInfo.imagemIndex, rowIndex, ((produtoRealmRealmProxyInterface) object).realmGet$imagem(), false);
        }
    }

    public static long insertOrUpdate(Realm realm, br.com.stts.prevenda.produtoRealm object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(br.com.stts.prevenda.produtoRealm.class);
        long tableNativePtr = table.getNativePtr();
        produtoRealmColumnInfo columnInfo = (produtoRealmColumnInfo) realm.schema.getColumnInfo(br.com.stts.prevenda.produtoRealm.class);
        long rowIndex = OsObject.createRow(table);
        cache.put(object, rowIndex);
        String realmGet$codigo = ((produtoRealmRealmProxyInterface) object).realmGet$codigo();
        if (realmGet$codigo != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.codigoIndex, rowIndex, realmGet$codigo, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.codigoIndex, rowIndex, false);
        }
        String realmGet$descricao = ((produtoRealmRealmProxyInterface) object).realmGet$descricao();
        if (realmGet$descricao != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.descricaoIndex, rowIndex, realmGet$descricao, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.descricaoIndex, rowIndex, false);
        }
        String realmGet$preco = ((produtoRealmRealmProxyInterface) object).realmGet$preco();
        if (realmGet$preco != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.precoIndex, rowIndex, realmGet$preco, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.precoIndex, rowIndex, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.imagemIndex, rowIndex, ((produtoRealmRealmProxyInterface) object).realmGet$imagem(), false);
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(br.com.stts.prevenda.produtoRealm.class);
        long tableNativePtr = table.getNativePtr();
        produtoRealmColumnInfo columnInfo = (produtoRealmColumnInfo) realm.schema.getColumnInfo(br.com.stts.prevenda.produtoRealm.class);
        br.com.stts.prevenda.produtoRealm object = null;
        while (objects.hasNext()) {
            object = (br.com.stts.prevenda.produtoRealm) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            long rowIndex = OsObject.createRow(table);
            cache.put(object, rowIndex);
            String realmGet$codigo = ((produtoRealmRealmProxyInterface) object).realmGet$codigo();
            if (realmGet$codigo != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.codigoIndex, rowIndex, realmGet$codigo, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.codigoIndex, rowIndex, false);
            }
            String realmGet$descricao = ((produtoRealmRealmProxyInterface) object).realmGet$descricao();
            if (realmGet$descricao != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.descricaoIndex, rowIndex, realmGet$descricao, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.descricaoIndex, rowIndex, false);
            }
            String realmGet$preco = ((produtoRealmRealmProxyInterface) object).realmGet$preco();
            if (realmGet$preco != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.precoIndex, rowIndex, realmGet$preco, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.precoIndex, rowIndex, false);
            }
            Table.nativeSetLong(tableNativePtr, columnInfo.imagemIndex, rowIndex, ((produtoRealmRealmProxyInterface) object).realmGet$imagem(), false);
        }
    }

    public static br.com.stts.prevenda.produtoRealm createDetachedCopy(br.com.stts.prevenda.produtoRealm realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        br.com.stts.prevenda.produtoRealm unmanagedObject;
        if (cachedObject == null) {
            unmanagedObject = new br.com.stts.prevenda.produtoRealm();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        } else {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (br.com.stts.prevenda.produtoRealm) cachedObject.object;
            }
            unmanagedObject = (br.com.stts.prevenda.produtoRealm) cachedObject.object;
            cachedObject.minDepth = currentDepth;
        }
        produtoRealmRealmProxyInterface unmanagedCopy = (produtoRealmRealmProxyInterface) unmanagedObject;
        produtoRealmRealmProxyInterface realmSource = (produtoRealmRealmProxyInterface) realmObject;
        unmanagedCopy.realmSet$codigo(realmSource.realmGet$codigo());
        unmanagedCopy.realmSet$descricao(realmSource.realmGet$descricao());
        unmanagedCopy.realmSet$preco(realmSource.realmGet$preco());
        unmanagedCopy.realmSet$imagem(realmSource.realmGet$imagem());
        return unmanagedObject;
    }

    @Override
    @SuppressWarnings("ArrayToString")
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("produtoRealm = proxy[");
        stringBuilder.append("{codigo:");
        stringBuilder.append(realmGet$codigo() != null ? realmGet$codigo() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{descricao:");
        stringBuilder.append(realmGet$descricao());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{preco:");
        stringBuilder.append(realmGet$preco());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{imagem:");
        stringBuilder.append(realmGet$imagem());
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState<?> realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        produtoRealmRealmProxy aprodutoRealm = (produtoRealmRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aprodutoRealm.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aprodutoRealm.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aprodutoRealm.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }

}
