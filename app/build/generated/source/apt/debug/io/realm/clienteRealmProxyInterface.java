package io.realm;


public interface clienteRealmProxyInterface {
    public String realmGet$email();
    public void realmSet$email(String value);
    public String realmGet$nome();
    public void realmSet$nome(String value);
}
