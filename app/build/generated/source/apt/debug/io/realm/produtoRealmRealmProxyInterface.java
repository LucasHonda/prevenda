package io.realm;


public interface produtoRealmRealmProxyInterface {
    public String realmGet$codigo();
    public void realmSet$codigo(String value);
    public String realmGet$descricao();
    public void realmSet$descricao(String value);
    public String realmGet$preco();
    public void realmSet$preco(String value);
    public int realmGet$imagem();
    public void realmSet$imagem(int value);
}
