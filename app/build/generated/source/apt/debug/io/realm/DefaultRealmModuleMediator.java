package io.realm;


import android.util.JsonReader;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.RealmProxyMediator;
import io.realm.internal.Row;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

@io.realm.annotations.RealmModule
class DefaultRealmModuleMediator extends RealmProxyMediator {

    private static final Set<Class<? extends RealmModel>> MODEL_CLASSES;
    static {
        Set<Class<? extends RealmModel>> modelClasses = new HashSet<Class<? extends RealmModel>>();
        modelClasses.add(br.com.stts.prevenda.cliente.class);
        modelClasses.add(br.com.stts.prevenda.produtoCarrinho.class);
        modelClasses.add(br.com.stts.prevenda.produtoRealm.class);
        MODEL_CLASSES = Collections.unmodifiableSet(modelClasses);
    }

    @Override
    public Map<Class<? extends RealmModel>, OsObjectSchemaInfo> getExpectedObjectSchemaInfoMap() {
        Map<Class<? extends RealmModel>, OsObjectSchemaInfo> infoMap = new HashMap<Class<? extends RealmModel>, OsObjectSchemaInfo>();
        infoMap.put(br.com.stts.prevenda.cliente.class, io.realm.clienteRealmProxy.getExpectedObjectSchemaInfo());
        infoMap.put(br.com.stts.prevenda.produtoCarrinho.class, io.realm.produtoCarrinhoRealmProxy.getExpectedObjectSchemaInfo());
        infoMap.put(br.com.stts.prevenda.produtoRealm.class, io.realm.produtoRealmRealmProxy.getExpectedObjectSchemaInfo());
        return infoMap;
    }

    @Override
    public ColumnInfo validateTable(Class<? extends RealmModel> clazz, SharedRealm sharedRealm, boolean allowExtraColumns) {
        checkClass(clazz);

        if (clazz.equals(br.com.stts.prevenda.cliente.class)) {
            return io.realm.clienteRealmProxy.validateTable(sharedRealm, allowExtraColumns);
        }
        if (clazz.equals(br.com.stts.prevenda.produtoCarrinho.class)) {
            return io.realm.produtoCarrinhoRealmProxy.validateTable(sharedRealm, allowExtraColumns);
        }
        if (clazz.equals(br.com.stts.prevenda.produtoRealm.class)) {
            return io.realm.produtoRealmRealmProxy.validateTable(sharedRealm, allowExtraColumns);
        }
        throw getMissingProxyClassException(clazz);
    }

    @Override
    public List<String> getFieldNames(Class<? extends RealmModel> clazz) {
        checkClass(clazz);

        if (clazz.equals(br.com.stts.prevenda.cliente.class)) {
            return io.realm.clienteRealmProxy.getFieldNames();
        }
        if (clazz.equals(br.com.stts.prevenda.produtoCarrinho.class)) {
            return io.realm.produtoCarrinhoRealmProxy.getFieldNames();
        }
        if (clazz.equals(br.com.stts.prevenda.produtoRealm.class)) {
            return io.realm.produtoRealmRealmProxy.getFieldNames();
        }
        throw getMissingProxyClassException(clazz);
    }

    @Override
    public String getTableName(Class<? extends RealmModel> clazz) {
        checkClass(clazz);

        if (clazz.equals(br.com.stts.prevenda.cliente.class)) {
            return io.realm.clienteRealmProxy.getTableName();
        }
        if (clazz.equals(br.com.stts.prevenda.produtoCarrinho.class)) {
            return io.realm.produtoCarrinhoRealmProxy.getTableName();
        }
        if (clazz.equals(br.com.stts.prevenda.produtoRealm.class)) {
            return io.realm.produtoRealmRealmProxy.getTableName();
        }
        throw getMissingProxyClassException(clazz);
    }

    @Override
    public <E extends RealmModel> E newInstance(Class<E> clazz, Object baseRealm, Row row, ColumnInfo columnInfo, boolean acceptDefaultValue, List<String> excludeFields) {
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        try {
            objectContext.set((BaseRealm) baseRealm, row, columnInfo, acceptDefaultValue, excludeFields);
            checkClass(clazz);

            if (clazz.equals(br.com.stts.prevenda.cliente.class)) {
                return clazz.cast(new io.realm.clienteRealmProxy());
            }
            if (clazz.equals(br.com.stts.prevenda.produtoCarrinho.class)) {
                return clazz.cast(new io.realm.produtoCarrinhoRealmProxy());
            }
            if (clazz.equals(br.com.stts.prevenda.produtoRealm.class)) {
                return clazz.cast(new io.realm.produtoRealmRealmProxy());
            }
            throw getMissingProxyClassException(clazz);
        } finally {
            objectContext.clear();
        }
    }

    @Override
    public Set<Class<? extends RealmModel>> getModelClasses() {
        return MODEL_CLASSES;
    }

    @Override
    public <E extends RealmModel> E copyOrUpdate(Realm realm, E obj, boolean update, Map<RealmModel, RealmObjectProxy> cache) {
        // This cast is correct because obj is either
        // generated by RealmProxy or the original type extending directly from RealmObject
        @SuppressWarnings("unchecked") Class<E> clazz = (Class<E>) ((obj instanceof RealmObjectProxy) ? obj.getClass().getSuperclass() : obj.getClass());

        if (clazz.equals(br.com.stts.prevenda.cliente.class)) {
            return clazz.cast(io.realm.clienteRealmProxy.copyOrUpdate(realm, (br.com.stts.prevenda.cliente) obj, update, cache));
        }
        if (clazz.equals(br.com.stts.prevenda.produtoCarrinho.class)) {
            return clazz.cast(io.realm.produtoCarrinhoRealmProxy.copyOrUpdate(realm, (br.com.stts.prevenda.produtoCarrinho) obj, update, cache));
        }
        if (clazz.equals(br.com.stts.prevenda.produtoRealm.class)) {
            return clazz.cast(io.realm.produtoRealmRealmProxy.copyOrUpdate(realm, (br.com.stts.prevenda.produtoRealm) obj, update, cache));
        }
        throw getMissingProxyClassException(clazz);
    }

    @Override
    public void insert(Realm realm, RealmModel object, Map<RealmModel, Long> cache) {
        // This cast is correct because obj is either
        // generated by RealmProxy or the original type extending directly from RealmObject
        @SuppressWarnings("unchecked") Class<RealmModel> clazz = (Class<RealmModel>) ((object instanceof RealmObjectProxy) ? object.getClass().getSuperclass() : object.getClass());

        if (clazz.equals(br.com.stts.prevenda.cliente.class)) {
            io.realm.clienteRealmProxy.insert(realm, (br.com.stts.prevenda.cliente) object, cache);
        } else if (clazz.equals(br.com.stts.prevenda.produtoCarrinho.class)) {
            io.realm.produtoCarrinhoRealmProxy.insert(realm, (br.com.stts.prevenda.produtoCarrinho) object, cache);
        } else if (clazz.equals(br.com.stts.prevenda.produtoRealm.class)) {
            io.realm.produtoRealmRealmProxy.insert(realm, (br.com.stts.prevenda.produtoRealm) object, cache);
        } else {
            throw getMissingProxyClassException(clazz);
        }
    }

    @Override
    public void insert(Realm realm, Collection<? extends RealmModel> objects) {
        Iterator<? extends RealmModel> iterator = objects.iterator();
        RealmModel object = null;
        Map<RealmModel, Long> cache = new HashMap<RealmModel, Long>(objects.size());
        if (iterator.hasNext()) {
            //  access the first element to figure out the clazz for the routing below
            object = iterator.next();
            // This cast is correct because obj is either
            // generated by RealmProxy or the original type extending directly from RealmObject
            @SuppressWarnings("unchecked") Class<RealmModel> clazz = (Class<RealmModel>) ((object instanceof RealmObjectProxy) ? object.getClass().getSuperclass() : object.getClass());

            if (clazz.equals(br.com.stts.prevenda.cliente.class)) {
                io.realm.clienteRealmProxy.insert(realm, (br.com.stts.prevenda.cliente) object, cache);
            } else if (clazz.equals(br.com.stts.prevenda.produtoCarrinho.class)) {
                io.realm.produtoCarrinhoRealmProxy.insert(realm, (br.com.stts.prevenda.produtoCarrinho) object, cache);
            } else if (clazz.equals(br.com.stts.prevenda.produtoRealm.class)) {
                io.realm.produtoRealmRealmProxy.insert(realm, (br.com.stts.prevenda.produtoRealm) object, cache);
            } else {
                throw getMissingProxyClassException(clazz);
            }
            if (iterator.hasNext()) {
                if (clazz.equals(br.com.stts.prevenda.cliente.class)) {
                    io.realm.clienteRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(br.com.stts.prevenda.produtoCarrinho.class)) {
                    io.realm.produtoCarrinhoRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(br.com.stts.prevenda.produtoRealm.class)) {
                    io.realm.produtoRealmRealmProxy.insert(realm, iterator, cache);
                } else {
                    throw getMissingProxyClassException(clazz);
                }
            }
        }
    }

    @Override
    public void insertOrUpdate(Realm realm, RealmModel obj, Map<RealmModel, Long> cache) {
        // This cast is correct because obj is either
        // generated by RealmProxy or the original type extending directly from RealmObject
        @SuppressWarnings("unchecked") Class<RealmModel> clazz = (Class<RealmModel>) ((obj instanceof RealmObjectProxy) ? obj.getClass().getSuperclass() : obj.getClass());

        if (clazz.equals(br.com.stts.prevenda.cliente.class)) {
            io.realm.clienteRealmProxy.insertOrUpdate(realm, (br.com.stts.prevenda.cliente) obj, cache);
        } else if (clazz.equals(br.com.stts.prevenda.produtoCarrinho.class)) {
            io.realm.produtoCarrinhoRealmProxy.insertOrUpdate(realm, (br.com.stts.prevenda.produtoCarrinho) obj, cache);
        } else if (clazz.equals(br.com.stts.prevenda.produtoRealm.class)) {
            io.realm.produtoRealmRealmProxy.insertOrUpdate(realm, (br.com.stts.prevenda.produtoRealm) obj, cache);
        } else {
            throw getMissingProxyClassException(clazz);
        }
    }

    @Override
    public void insertOrUpdate(Realm realm, Collection<? extends RealmModel> objects) {
        Iterator<? extends RealmModel> iterator = objects.iterator();
        RealmModel object = null;
        Map<RealmModel, Long> cache = new HashMap<RealmModel, Long>(objects.size());
        if (iterator.hasNext()) {
            //  access the first element to figure out the clazz for the routing below
            object = iterator.next();
            // This cast is correct because obj is either
            // generated by RealmProxy or the original type extending directly from RealmObject
            @SuppressWarnings("unchecked") Class<RealmModel> clazz = (Class<RealmModel>) ((object instanceof RealmObjectProxy) ? object.getClass().getSuperclass() : object.getClass());

            if (clazz.equals(br.com.stts.prevenda.cliente.class)) {
                io.realm.clienteRealmProxy.insertOrUpdate(realm, (br.com.stts.prevenda.cliente) object, cache);
            } else if (clazz.equals(br.com.stts.prevenda.produtoCarrinho.class)) {
                io.realm.produtoCarrinhoRealmProxy.insertOrUpdate(realm, (br.com.stts.prevenda.produtoCarrinho) object, cache);
            } else if (clazz.equals(br.com.stts.prevenda.produtoRealm.class)) {
                io.realm.produtoRealmRealmProxy.insertOrUpdate(realm, (br.com.stts.prevenda.produtoRealm) object, cache);
            } else {
                throw getMissingProxyClassException(clazz);
            }
            if (iterator.hasNext()) {
                if (clazz.equals(br.com.stts.prevenda.cliente.class)) {
                    io.realm.clienteRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(br.com.stts.prevenda.produtoCarrinho.class)) {
                    io.realm.produtoCarrinhoRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(br.com.stts.prevenda.produtoRealm.class)) {
                    io.realm.produtoRealmRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else {
                    throw getMissingProxyClassException(clazz);
                }
            }
        }
    }

    @Override
    public <E extends RealmModel> E createOrUpdateUsingJsonObject(Class<E> clazz, Realm realm, JSONObject json, boolean update)
        throws JSONException {
        checkClass(clazz);

        if (clazz.equals(br.com.stts.prevenda.cliente.class)) {
            return clazz.cast(io.realm.clienteRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        }
        if (clazz.equals(br.com.stts.prevenda.produtoCarrinho.class)) {
            return clazz.cast(io.realm.produtoCarrinhoRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        }
        if (clazz.equals(br.com.stts.prevenda.produtoRealm.class)) {
            return clazz.cast(io.realm.produtoRealmRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        }
        throw getMissingProxyClassException(clazz);
    }

    @Override
    public <E extends RealmModel> E createUsingJsonStream(Class<E> clazz, Realm realm, JsonReader reader)
        throws IOException {
        checkClass(clazz);

        if (clazz.equals(br.com.stts.prevenda.cliente.class)) {
            return clazz.cast(io.realm.clienteRealmProxy.createUsingJsonStream(realm, reader));
        }
        if (clazz.equals(br.com.stts.prevenda.produtoCarrinho.class)) {
            return clazz.cast(io.realm.produtoCarrinhoRealmProxy.createUsingJsonStream(realm, reader));
        }
        if (clazz.equals(br.com.stts.prevenda.produtoRealm.class)) {
            return clazz.cast(io.realm.produtoRealmRealmProxy.createUsingJsonStream(realm, reader));
        }
        throw getMissingProxyClassException(clazz);
    }

    @Override
    public <E extends RealmModel> E createDetachedCopy(E realmObject, int maxDepth, Map<RealmModel, RealmObjectProxy.CacheData<RealmModel>> cache) {
        // This cast is correct because obj is either
        // generated by RealmProxy or the original type extending directly from RealmObject
        @SuppressWarnings("unchecked") Class<E> clazz = (Class<E>) realmObject.getClass().getSuperclass();

        if (clazz.equals(br.com.stts.prevenda.cliente.class)) {
            return clazz.cast(io.realm.clienteRealmProxy.createDetachedCopy((br.com.stts.prevenda.cliente) realmObject, 0, maxDepth, cache));
        }
        if (clazz.equals(br.com.stts.prevenda.produtoCarrinho.class)) {
            return clazz.cast(io.realm.produtoCarrinhoRealmProxy.createDetachedCopy((br.com.stts.prevenda.produtoCarrinho) realmObject, 0, maxDepth, cache));
        }
        if (clazz.equals(br.com.stts.prevenda.produtoRealm.class)) {
            return clazz.cast(io.realm.produtoRealmRealmProxy.createDetachedCopy((br.com.stts.prevenda.produtoRealm) realmObject, 0, maxDepth, cache));
        }
        throw getMissingProxyClassException(clazz);
    }

}
