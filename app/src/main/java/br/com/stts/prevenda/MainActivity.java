package br.com.stts.prevenda;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private ImageView ivCoca;
    private ImageView ruffles;
    private ImageView pepsi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Bloco adicionado na seleção de layouts do android
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        // Fim do bloco gerado automaticamente

        ivCoca = (ImageView) findViewById(R.id.coca);
        ruffles = (ImageView) findViewById(R.id.ruffles);
        pepsi = (ImageView) findViewById(R.id.pepsi);

        ivCoca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), vendas.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("codigo", "4");
                bundle.putSerializable("preco", "4,50");
                bundle.putSerializable("imagem", R.drawable.prod4);
                bundle.putSerializable("descricao", "Coca Lata 350ml");
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        ruffles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), vendas.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("codigo", "1");
                bundle.putSerializable("preco", "5,50");
                bundle.putSerializable("imagem", R.drawable.prod1);
                bundle.putSerializable("descricao", "Batata Ruffles Original 96g");
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        pepsi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), vendas.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("codigo", "5");
                bundle.putSerializable("preco", "3,50");
                bundle.putSerializable("imagem", R.drawable.prod5);
                bundle.putSerializable("descricao", "Pepsi Lata 350ml");
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_prods) {
            Intent intent = new Intent(this, produtos.class);
            startActivity(intent);
        } else if(id == R.id.nav_venda){
            Intent intent = new Intent(this, vendas.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }




}
