package br.com.stts.prevenda;

import android.content.Context;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by lucas on 27/11/17.
 */

public class produtoCarrinhoListar implements IProdutoCarrinho {

    private Realm realm;
    private Context context;

    public produtoCarrinhoListar(Context context){this.context = context;}

    @Override
    public void listarProdutos(OnListarProdutosCallback callback) {
        try{
            RealmResults<produtoRealm> produtos = realm.where(produtoRealm.class).findAll();
            if(callback != null){
                callback.onSucesso(produtos);
            }
        }catch (Throwable t){
            t.printStackTrace();
            if(callback != null){
                callback.onErro(t);
            }
        }
    }

    @Override
    public List<produtoCarrinho> listarProdutos() {
        realm.init(context);
        realm = Realm.getDefaultInstance();
        try{
            return realm.where(produtoCarrinho.class).findAll();
        }catch (Throwable t){
            t.printStackTrace();
            return null;
        }
    }

    @Override
    public produtoCarrinho consultarProduto(String codigo) {
        realm.init(context);
        realm = Realm.getDefaultInstance();
        if(codigo == null || codigo.isEmpty()){
            return null;
        }
        try {
            produtoCarrinho produto = realm.where(produtoCarrinho.class).equalTo("codigo", codigo).findFirst();
            if(produto == null) return null;
            else return realm.copyFromRealm(produto);
        }catch (Throwable t){
            t.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean removerProduto(final produtoCarrinho produtoCarrinho) {
        realm.init(context);
        realm = Realm.getDefaultInstance();
        if(produtoCarrinho == null){
            return false;
        }
        try{
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                realm.where(produtoCarrinho.class).equalTo("codigo", produtoCarrinho.getCodigo()).findAll().deleteFirstFromRealm();
                }
            });
            return true;
        }catch (Throwable t){
            t.printStackTrace();
            return false;
        }
    }

    public void criar(final String codigo, final String descricao, final String preco, final String quantidade,final int imagem){
        realm.init(context);
        realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                produtoCarrinho lista = realm.createObject(produtoCarrinho.class);

                lista.setCodigo(codigo);
                lista.setDescricao(descricao);
                lista.setPreco(preco);
                lista.setQuantidade(quantidade);
                lista.setImagem(imagem);
            }
        });
    }

    public boolean alterarProduto(final produtoCarrinho alterar, final String quantidade){
        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    produtoCarrinho original = realm.where(produtoCarrinho.class).equalTo("codigo", alterar.getCodigo()).findFirst();
                    copiar(original, quantidade);
                }
            });
            return true;
        }catch (Throwable t){
            t.printStackTrace();
            return false;
        }
    }

    private void copiar(produtoCarrinho original, String quantidade){
        original.setQuantidade(quantidade);

    }

    public void clearDB() {
        realm.init(context);
        realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.delete(produtoCarrinho.class);
            }
        });
    }
}
