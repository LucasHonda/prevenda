package br.com.stts.prevenda;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.Required;

/**
 * Created by lucas on 30/11/17.
 */

public class cliente extends RealmObject{

    @Index
    private String email;
    @Required
    private String nome;

    public cliente() {}

    public cliente(String email, String nome) {
        super();
        this.email = email;
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}


