package br.com.stts.prevenda;

import java.util.List;

/**
 * Created by lucas on 22/11/17.
 */

public interface IProdutoRealm {
    public void listarProdutos(OnListarProdutosCallback callback);
    public List<produtoRealm> listarProdutos();
    public produtoRealm consultarProduto(String string);
}
