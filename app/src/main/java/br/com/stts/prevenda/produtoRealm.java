package br.com.stts.prevenda;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.Required;

/**
 * Created by lucas on 22/11/17.
 */

public class produtoRealm extends RealmObject {

    @Index
    private String codigo;
    @Required
    private String descricao;
    @Required
    private String preco;

    private int imagem;

    public produtoRealm(){}

    public produtoRealm(String codigo, String descricao, String preco, int imagem) {
        super();
        this.codigo = codigo;
        this.descricao = descricao;
        this.preco = preco;
        this.imagem = imagem;
    }

    public String getCodigo() { return codigo; }

    public void setCodigo(String codigo) { this.codigo = codigo; }

    public String getDescricao() { return descricao; }

    public void setDescricao(String descricao) { this.descricao = descricao; }

    public String getPreco() { return preco; }

    public void setPreco(String preco) { this.preco = preco; }

    public int getImagem() { return imagem; }

    public void setImagem(int imagem) { this.imagem = imagem; }
}
