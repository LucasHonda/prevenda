package br.com.stts.prevenda;

import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lucas on 24/11/17.
 */

public class vendas extends AppCompatActivity{

    // declara��o de v�riaveis globais
    private List<produtoCarrinho> fecharCompra = new ArrayList<>();
    private String total;
    private String email;
    private String nome;
    private List<cliente> cli = new ArrayList<>();

    private List<produtoCarrinho> produtoCarrinhos = new ArrayList<>();
    private listaCarrinhoAdapter lstCarrinho;
    private boolean visualizar;

    private String codigo;
    private String descricao;
    private String preco;
    private String quantidade;
    private int imagem;
    private String subtotal;


    private EditText edQtd;
    private EditText edPreco;
    private EditText edDescricao;
    private ImageView ivProduto;
    private EditText edQtdItens;
    private EditText edSubTotal;
    private Button btnFinalizar;

    ProgressDialog waitDialog;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vendas);

        // declarando os campos do layout
        btnFinalizar = (Button) findViewById(R.id.btn_enviar);
        edQtd = (EditText) findViewById(R.id.ed_qtd);
        edPreco = (EditText) findViewById(R.id.ed_preco);
        edDescricao = (EditText) findViewById(R.id.ed_descricao);
        ivProduto = (ImageView) findViewById(R.id.iv_prod);
        edQtdItens = (EditText) findViewById(R.id.ed_qtd_itens);
        edSubTotal = (EditText) findViewById(R.id.ed_subtotal);
        
        // Coleta de informa��es extras
        Bundle bundle = getIntent().getExtras();
        
        // Teste de informa��es extras
        if(bundle != null){
            codigo = bundle.getSerializable("codigo").toString();
            descricao = bundle.getSerializable("descricao").toString();
            preco = bundle.getSerializable("preco").toString().replace(",", ".");
            imagem = (int) bundle.getSerializable("imagem");

            edPreco.setText(String.format("R$ %s", bundle.getSerializable("preco").toString().replace(",", ".")));
            ivProduto.setImageResource((Integer) bundle.getSerializable("imagem"));
            edDescricao.setText(bundle.getSerializable("descricao").toString());
            edQtdItens.setText("1");
            quantidade = "1";
            atualiza();
            edQtd.requestFocus();
            visualizar = false;
            btnFinalizar.setEnabled(false);
        } else{
            Toast.makeText(this, R.string.mensagem_adicionar_produto, Toast.LENGTH_LONG).show();
            edQtd.setInputType(InputType.TYPE_NULL);
            visualizar = true;
            // Listagem dos produtos que est�o no carrinho
            final produtoCarrinhoListar produtoCarrinhoListar = new produtoCarrinhoListar(this);

            produtoCarrinhos = produtoCarrinhoListar.listarProdutos();

            lstCarrinho = new listaCarrinhoAdapter(this, produtoCarrinhos);

            final ListView listaCarrinho = (ListView) findViewById(R.id.lst_produtos);

            if(produtoCarrinhos != null){
                listaCarrinho.setAdapter(lstCarrinho);
                for(int i=0; i<produtoCarrinhos.size(); i++){
                    String val1 = produtoCarrinhos.get(i).getPreco().toString().replace("," ,".");
                    String val2 = produtoCarrinhos.get(i).getQuantidade().toString();
                    BigDecimal val3 = BigDecimal.valueOf(Double.parseDouble(val1) * Double.parseDouble(val2));
                    if(subtotal != null){
                        subtotal = String.valueOf(BigDecimal.valueOf(Double.parseDouble(subtotal)).add(val3));
                    } else{
                        subtotal = val3.toString();
                    }

                }
                edSubTotal.setText(subtotal);
                edQtdItens.setText(String.valueOf(produtoCarrinhos.size()));
            } else{
                Toast.makeText(this, R.string.mensagem_sem_carrinho, Toast.LENGTH_LONG).show();
            }

            listaCarrinho.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    produtoCarrinho produto = produtoCarrinhoListar.consultarProduto(produtoCarrinhos.get(position).getCodigo().toString());
                    if(produto != null){
                        codigo = produto.getCodigo().toString();
                        preco = produto.getPreco().toString().replace(",", ".");

                        edPreco.setText(String.format("R$ %s" , produto.getPreco().toString().replace(",", ".")));
                        edQtd.setText(produto.getQuantidade());
                        edQtd.setInputType(InputType.TYPE_CLASS_NUMBER);
                        edQtd.requestFocus();
                        ivProduto.setImageResource(produto.getImagem());
                        edDescricao.setText(produto.getDescricao());

                    } else{
                        Toast.makeText(getApplicationContext(), R.string.mensagem_lista_erro, Toast.LENGTH_LONG).show();
                    }
                }
            });
        }

        edQtd.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {
                    if(Integer.parseInt(edQtd.getText().toString()) == 0){
                        Toast.makeText(getApplicationContext(), R.string.mensagem_qtd_zero, Toast.LENGTH_LONG).show();
                        visualizar = true;
                        onCancelar();
                    }
                    if(edPreco.getText().toString() != null){
                        atualiza();
                        edQtdItens.setText(edQtd.getText().toString());
                        quantidade = edQtd.getText().toString();
                        ((InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(edQtd.getWindowToken(), 0);
                        onOk();
                    } else{
                        ((InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(edQtd.getWindowToken(), 0);
                        Toast.makeText(getApplicationContext(), R.string.mensagem_produto_erro, Toast.LENGTH_LONG).show();
                    }
                    return true;
                }
                return false;
            }
        });
        
        // L�gica da finaliza��o
        final produtoCarrinhoListar prod = new produtoCarrinhoListar(getApplicationContext());

        fecharCompra = prod.listarProdutos();

        final clienteListar cliente = new clienteListar(getApplicationContext());
        cli = cliente.consultarCliente();
        email = cli.get(0).getEmail().toString();
        nome = cli.get(0).getNome().toString();

        btnFinalizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(fecharCompra.isEmpty()){
                    Toast.makeText(getApplicationContext(), R.string.mensagem_erro_carrinho, Toast.LENGTH_LONG).show();
                } else {
                    
                    NotificationCompat.Builder nBuilder =
                        new NotificationCompat.Builder(getApplicationContext())
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentTitle("Lista de compras de " + nome)
                            .setContentText("Enviado para o email: " + email);

                    NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                    notificationManager.notify(1 , nBuilder.build());


                    final StringBuilder carrinho = new StringBuilder();
                    carrinho.append("Lista de Compras" + " - " + "Cliente " + nome + "\n");
                    carrinho.append("-----------------------" + "\n");
                    carrinho.append("\n");
                    for(int i=0; i<fecharCompra.size(); i++){
                        carrinho.append(fecharCompra.get(i).getCodigo() + " - " + fecharCompra.get(i).getDescricao() + " R$ " + fecharCompra.get(i).getPreco() + " x" + fecharCompra.get(i).getQuantidade() + "\n");
                        carrinho.append("\n");
                    }
                    for(int t=0; t<fecharCompra.size(); t++){
                        String val1 = fecharCompra.get(t).getPreco().toString().replace("," ,".");
                        String val2 = fecharCompra.get(t).getQuantidade().toString();
                        BigDecimal val3 = BigDecimal.valueOf(Double.parseDouble(val1) * Double.parseDouble(val2));
                        if(total != null){
                            total = String.valueOf(BigDecimal.valueOf(Double.parseDouble(total)).add(val3));
                        } else{
                            total = val3.toString();
                        }
                    }
                    carrinho.append("-----------------------");
                    carrinho.append("\n");
                    carrinho.append("Total: " + "R$ " + total);

                    emailTask emailTask = new emailTask(email, carrinho.toString());
                    emailTask.execute();
                }
            }
        });
    }

    //m�todo utilizado na adi��o de produto, cliente verifica quanto vai pagar por produto
    private void atualiza(){
        if(edSubTotal.getText().toString().isEmpty()){
            edSubTotal.setText(String.format("R$ %s", String.valueOf(BigDecimal.valueOf(Double.parseDouble(preco)).multiply(BigDecimal.valueOf(Double.parseDouble(edQtd.getText().toString()))))));
        } else{
            edSubTotal.setText(String.format("R$ %s", String.valueOf((BigDecimal.valueOf(Double.parseDouble(preco)).multiply(BigDecimal.valueOf(Double.parseDouble(edQtd.getText().toString())))))));
        }
    }

    private void onOk(){
        // quando visualizar est� com o valor true, o mesmo ir� definir se o produto pode ou n�o ser editado.
        if(visualizar == true){
            produtoCarrinhoListar produtoCarrinhoListar = new produtoCarrinhoListar(getApplicationContext());
            quantidade = edQtd.getText().toString();
            if(produtoCarrinhoListar.alterarProduto(produtoCarrinhoListar.consultarProduto(codigo), quantidade)){
                Toast.makeText(getApplicationContext(), R.string.mensagem_produto_salvo, Toast.LENGTH_LONG).show();

                produtoCarrinhos = produtoCarrinhoListar.listarProdutos();

                lstCarrinho = new listaCarrinhoAdapter(this, produtoCarrinhos);

                final ListView listaCarrinho = (ListView) findViewById(R.id.lst_produtos);

                listaCarrinho.setAdapter(lstCarrinho);

                edQtd.setText("0");
                edQtd.setInputType(InputType.TYPE_NULL);
                edPreco.setText("");
                edDescricao.setText("");
                ivProduto.setImageResource(R.drawable.question);
                edQtdItens.setText(String.valueOf(produtoCarrinhos.size()));
                subtotal = null;
                for(int i=0; i<produtoCarrinhos.size(); i++){
                    String val1 = produtoCarrinhos.get(i).getPreco().toString().replace("," ,".");
                    String val2 = produtoCarrinhos.get(i).getQuantidade().toString();
                    BigDecimal val3 = BigDecimal.valueOf(Double.parseDouble(val1) * Double.parseDouble(val2));
                    if(subtotal != null){
                        subtotal = String.valueOf(BigDecimal.valueOf(Double.parseDouble(subtotal)).add(val3));
                    } else{
                        subtotal = val3.toString();
                    }
                }
                edSubTotal.setText(subtotal);
            } else{
                Toast.makeText(getApplicationContext(), R.string.mensagem_produto_salvo_erro, Toast.LENGTH_LONG).show();
            }
        } else {
            if(Integer.parseInt(edQtd.getText().toString()) == 0){                
                onCancelar();
            }

            produtoCarrinhoListar produtoCarrinhoListar = new produtoCarrinhoListar(getApplicationContext());
            produtoCarrinhoListar.criar(codigo, descricao, preco.replace("." , ","), quantidade, imagem);

            List<produtoCarrinho> teste = produtoCarrinhoListar.listarProdutos();
            System.out.println(teste);
            Toast.makeText(this, R.string.mensagem_adicionado_sucesso, Toast.LENGTH_LONG).show();
            finish();
        }
    }

    private void onCancelar(){
        produtoCarrinhoListar produtoCarrinhoListar = new produtoCarrinhoListar(getApplicationContext());
        if(visualizar == true){
            produtoCarrinho prod = produtoCarrinhoListar.consultarProduto(codigo);
            if(produtoCarrinhoListar.removerProduto(prod)){
                Toast.makeText(getApplicationContext(), R.string.mensagem_produto_excluido, Toast.LENGTH_LONG).show();

                produtoCarrinhos = produtoCarrinhoListar.listarProdutos();

                lstCarrinho = new listaCarrinhoAdapter(this, produtoCarrinhos);

                final ListView listaCarrinho = (ListView) findViewById(R.id.lst_produtos);

                listaCarrinho.setAdapter(lstCarrinho);

                edQtd.setText("0");
                edQtd.setInputType(InputType.TYPE_NULL);
                edPreco.setText("");
                edDescricao.setText("");
                ivProduto.setImageResource(R.drawable.question);
                edQtdItens.setText(String.valueOf(produtoCarrinhos.size()));
                subtotal = null;
                for(int i=0; i<produtoCarrinhos.size(); i++){
                    String val1 = produtoCarrinhos.get(i).getPreco().toString().replace("," ,".");
                    String val2 = produtoCarrinhos.get(i).getQuantidade().toString();
                    BigDecimal val3 = BigDecimal.valueOf(Double.parseDouble(val1) * Double.parseDouble(val2));
                    if(subtotal != null){
                        subtotal = String.valueOf(BigDecimal.valueOf(Double.parseDouble(subtotal)).add(val3));
                    } else{
                        subtotal = val3.toString();
                    }
                }
                edSubTotal.setText(subtotal);
            } else{
                Toast.makeText(getApplicationContext(), R.string.mensagem_produto_excluido_erro, Toast.LENGTH_LONG).show();
            }
        } else{
            Toast.makeText(this, R.string.mensagem_produto_cancelado, Toast.LENGTH_LONG).show();
            finish();
        }
    }
    
    // Bot�es na action bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.acoes, menu);
        return super.onCreateOptionsMenu(menu);
    }

    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.bt_ok) {
            onOk();
            return true;
        } else if (id == R.id.bt_cancelar) {
            onCancelar();
        }

        return super.onOptionsItemSelected(item);

    }

    // Task de envio de email
    private class emailTask extends AsyncTask<Void, Void, Boolean> {

        public String email;
        public String produtos;

        public Boolean sucesso;

        public emailTask(String email, String produtos) {
            this.email = email;
            this.produtos = produtos;
        }

        @Override
        protected Boolean doInBackground(Void[] params) {
            if(enviar(email, produtos)){
                sucesso = true;
                return true;
            } else{
                sucesso = false;
                return false;
            }
        }

        private boolean enviar(String email, String produtos){
            try {
                emailSender sender = new emailSender("android@pisolucoes.com.br", "turmaI60#");
                if(sender.sendMail("Lista da sua compra", produtos, "android@pisolucoes.com.br", email)){
                    return true;
                } else {
                    return false;
                }
            } catch (Exception e) {
                Log.e("SendEmail: ", e.getMessage(), e);
            }
            return false;
        }

        private void finalizar(){
            final produtoCarrinhoListar prod = new produtoCarrinhoListar(getApplicationContext());
            prod.clearDB();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            waitDialog = ProgressDialog.show(vendas.this, "Aguarde", "Enviando...");
        }

        @Override
        protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);
            if(sucesso){
                waitDialog.dismiss();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        finalizar();
                        total = null;
                        Toast.makeText(getApplicationContext(), R.string.mensagem_venda_finalizar, Toast.LENGTH_LONG).show();
                        finish();
                    }
                });
            } else{
                waitDialog.dismiss();
                Toast.makeText(getApplicationContext(), R.string.mensagem_erro_email, Toast.LENGTH_LONG).show();
            }
        }

    }
}
