package br.com.stts.prevenda;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by lucas on 22/11/17.
 */

public class listaProdutoAdapter extends ArrayAdapter<produtoRealm> {

    private Activity activity;

    private List<produtoRealm> produtos;

    public listaProdutoAdapter(@NonNull Activity context,@NonNull List<produtoRealm> produtos) {
        super(context, R.layout.model_prod, produtos);

        this.produtos = produtos;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        if (convertView == null) {

            convertView = LayoutInflater.from(getContext()).inflate(R.layout.model_prod, parent, false);

        }

        ImageView prodImg = convertView.findViewById(R.id.iv_prod);
        TextView prodDesc = convertView.findViewById(R.id.descricao);
        TextView prodPreco = convertView.findViewById(R.id.preco);

        produtoRealm prod = produtos.get(position);

        prodImg.setImageResource(prod.getImagem());
        prodDesc.setText(prod.getCodigo() + " - " + prod.getDescricao());
        prodPreco.setText(String.format("R$ %s", prod.getPreco().replace(",", ".")));

        return convertView;

    }
}
