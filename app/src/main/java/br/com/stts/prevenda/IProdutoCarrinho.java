package br.com.stts.prevenda;

import java.util.List;

/**
 * Created by lucas on 27/11/17.
 */

public interface IProdutoCarrinho {
    public void listarProdutos(OnListarProdutosCallback callback);
    public List<produtoCarrinho> listarProdutos();
    public produtoCarrinho consultarProduto(String string);

    public boolean removerProduto(produtoCarrinho produtoCarrinho);
}
