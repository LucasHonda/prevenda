package br.com.stts.prevenda;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by lucas on 30/11/17.
 */

public class login extends AppCompatActivity{


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        final EditText edEmail = (EditText) findViewById(R.id.ed_email);
        final EditText edNome = (EditText) findViewById(R.id.ed_nome);

        Button btEntrar = (Button) findViewById(R.id.btn_entrar);

        final clienteListar clienteListar = new clienteListar(this);

        btEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edEmail.getText().toString().isEmpty() || edNome.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext(), R.string.mensagem_campos_vazios, Toast.LENGTH_LONG).show();
                    if(edEmail.getText().toString().isEmpty()) edEmail.requestFocus();
                    else edNome.requestFocus();
                } else{
                    clienteListar.clearDB();
                    clienteListar.criar(edEmail.getText().toString(), edNome.getText().toString());
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
            }
        });
    }
}
