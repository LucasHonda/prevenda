package br.com.stts.prevenda;

import android.content.Context;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by lucas on 22/11/17.
 */

public class produtoRealmListar implements IProdutoRealm {

    private Realm realm;
    private Context context;

    public produtoRealmListar(Context context) {
        this.context = context;
    }

    @Override
    public void listarProdutos(OnListarProdutosCallback callback) {
        try{
            RealmResults<produtoRealm> produtos = realm.where(produtoRealm.class).findAll();
            if(callback != null){
                callback.onSucesso(produtos);
            }
        }catch (Throwable t){
            t.printStackTrace();
            if(callback != null){
                callback.onErro(t);
            }
        }
    }

    @Override
    public List<produtoRealm> listarProdutos() {
        try{
            return realm.copyFromRealm(realm.where(produtoRealm.class).findAll());
        }catch (Throwable t){
            t.printStackTrace();
            return null;
        }
    }

    @Override
    public produtoRealm consultarProduto(String codigo) {
        if(codigo == null || codigo.isEmpty()){
            return null;
        }
        try {
            produtoRealm produto = realm.where(produtoRealm.class).equalTo("codigo", codigo).findFirst();

            if(produto == null) return null;
            else return realm.copyFromRealm(produto);
        }catch (Throwable t){
            t.printStackTrace();
            return null;
        }
    }

    /*
    *
    * Adição de produtos via mocado, ou seja fixo.
    *
    * */

    public void clearDB() {
        realm.init(context);
        realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.delete(produtoRealm.class);
            }
        });
    }



    public void criarProdutos(){
        clearDB();
        criar("1","Batata Ruffles Original 96g","5,50",R.drawable.prod1);
        criar("2","Doritos Queijo 96g","6,00", R.drawable.prod2);
        criar("3","Água Mineral 510ml","2,00",R.drawable.prod3);
        criar("4","Coca Lata 350ml","4,50",R.drawable.prod4);
        criar("5","Pepsi Lata 350ml","3,50",R.drawable.prod5);
        criar("6","Leite Integral 1L","2,10",R.drawable.prod6);
        criar("7","Farinha de Trigo 1Kg","4,00",R.drawable.prod7);
        criar("8","Ovos Brancos 1 dúzia","7,00",R.drawable.prod8);
        criar("9","Cenoura 1Kg","2,30",R.drawable.prod9);
        criar("10","Batata 1Kg","2,00",R.drawable.prod10);
    }

    private void criar(final String codigo, final String descricao, final String preco, final int imagem){
        realm.init(context);
        realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                produtoRealm lista = realm.createObject(produtoRealm.class);

                lista.setCodigo(codigo);
                lista.setDescricao(descricao);
                lista.setPreco(preco);
                lista.setImagem(imagem);
            }
        });

    }
}
