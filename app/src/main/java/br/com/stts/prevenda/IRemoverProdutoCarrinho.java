package br.com.stts.prevenda;

/**
 * Created by lucas on 27/11/17.
 */

public interface IRemoverProdutoCarrinho {

    public void onErro(Throwable t);
    public void onSucesso(produtoCarrinho produtoCarrinho);

}
