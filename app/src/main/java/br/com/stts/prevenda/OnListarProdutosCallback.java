package br.com.stts.prevenda;

import java.util.List;

/**
 * Created by lucas on 22/11/17.
 */

public interface OnListarProdutosCallback {
    public void onErro(Throwable t);
    public void onSucesso(List<produtoRealm> produtos);
}
