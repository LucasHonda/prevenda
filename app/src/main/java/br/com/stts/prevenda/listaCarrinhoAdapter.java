package br.com.stts.prevenda;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by lucas on 27/11/17.
 */

public class listaCarrinhoAdapter extends ArrayAdapter<produtoCarrinho>{
    private Activity activity;
    private List<produtoCarrinho> produtoCarrinhos;

    public listaCarrinhoAdapter(@NonNull Activity context, @NonNull List<produtoCarrinho> produtoCarrinhos) {
        super(context, R.layout.model_car, produtoCarrinhos);

        this.produtoCarrinhos = produtoCarrinhos;
    }


    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        if (convertView == null) {

            convertView = LayoutInflater.from(getContext()).inflate(R.layout.model_car, parent, false);

        }

        ImageView prodImg = convertView.findViewById(R.id.iv_prod);
        TextView prodDesc = convertView.findViewById(R.id.descricao);
        TextView prodPreco = convertView.findViewById(R.id.preco);
        TextView prodQuantidade = convertView.findViewById(R.id.quantidade);

        produtoCarrinho prod = produtoCarrinhos.get(position);

        prodImg.setImageResource(prod.getImagem());
        prodDesc.setText(prod.getCodigo() + " - " + prod.getDescricao());
        prodPreco.setText(String.format("R$ %s", prod.getPreco().replace(",", ".")));
        prodQuantidade.setText(String.format("x %s", prod.getQuantidade()));

        return convertView;

    }
}
