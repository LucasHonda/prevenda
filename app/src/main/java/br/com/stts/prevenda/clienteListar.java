package br.com.stts.prevenda;

import android.content.Context;

import java.util.List;

import io.realm.Realm;

/**
 * Created by lucas on 30/11/17.
 */

public class clienteListar {

    private Realm realm;
    private Context context;


    public clienteListar(Context context) {
        this.context = context;
    }

    public List<cliente> consultarCliente() {
        realm.init(context);
        realm = Realm.getDefaultInstance();
        try{
            return realm.where(cliente.class).findAll();
        }catch (Throwable t){
            t.printStackTrace();
            return null;
        }
    }

    public void criar(final String email, final String nome){
        realm.init(context);
        realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                cliente cliente = realm.createObject(cliente.class);

                cliente.setEmail(email);
                cliente.setNome(nome);
            }
        });
    }

    public void clearDB() {
        realm.init(context);
        realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.delete(cliente.class);
            }
        });
    }
}

