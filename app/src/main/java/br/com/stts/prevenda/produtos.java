package br.com.stts.prevenda;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lucas on 22/11/17.
 */

public class produtos extends AppCompatActivity{

    private List<produtoRealm> produtos = new ArrayList<>();

    private listaProdutoAdapter lst;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.produtos);

        // Objeto que realizará todos as procedures de "Banco"
        final produtoRealmListar prod = new produtoRealmListar(this);
        // Inicialização dos produtos
        prod.criarProdutos();

        produtos = prod.listarProdutos();

        lst = new listaProdutoAdapter(this, produtos);

        final ListView lstProds = (ListView) findViewById(R.id.lst_produtos);

        lstProds.setAdapter(lst);

        lstProds.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                produtoRealm Produto = prod.consultarProduto(produtos.get(position).getCodigo().toString());
                if(Produto != null){
                    Intent intent = new Intent(getApplicationContext(), vendas.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("codigo", Produto.getCodigo());
                    bundle.putSerializable("preco", Produto.getPreco());
                    bundle.putSerializable("imagem", Produto.getImagem());
                    bundle.putSerializable("descricao", Produto.getDescricao());
                    intent.putExtras(bundle);
                    startActivity(intent);
                } else{
                    Toast.makeText(getApplicationContext(), R.string.mensagem_lista_erro, Toast.LENGTH_LONG).show();
                }
            }
        });

    }

}
